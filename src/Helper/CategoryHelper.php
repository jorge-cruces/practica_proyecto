<?php


namespace App\Helper;


abstract class CategoryHelper
{
    public static $categoryList = [
        'Ciencia',
        'Deportes',
        'Humanidades',
        'Urbano',
        'Espiritual',
        'Actualidad',
        'Internacional'
    ];

}