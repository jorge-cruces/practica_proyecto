<?php

namespace App\Form;

use App\Entity\News;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class NewsFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {



        $builder
            ->add('category', null, $this->generateLabel('Categoria'))
            ->add('title',null, $this->generateLabel('Titulo'))
            ->add('body',null,[
                'attr' => ['rows' => 10],
                'label' => 'Cuerpo de la noticia'
            ])
            ->add('file',null,[
              'label' => 'Imagen',
              'is_image' => 'true'
            ])
            ->add('enabled', null,$this->generateLabel('¿Visible?'))
            ->add('description',null, $this->generateLabel('Descripcion'))
            ->add('createdAt', DateType::class, [
                'widget' => 'single_text',
                'disabled' => true,
                'data' => new \DateTime,
                'label' => 'Creado el dia:'
            ]);

            if($options['edit']){

                $builder->add('updatedAt', DateType::class, [
                    'widget' => 'single_text',
                    'disabled' => true,
                    'label' => 'Actualizado el dia:'
                ]);

            }



    }

    private function generateLabel($string)
    {
        return ['label' => $string];
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => News::class,
            'edit' => false
        ]);
    }
}
