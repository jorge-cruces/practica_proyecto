<?php

namespace App\Controller\User;

use App\Entity\News;
use App\Form\NewsFormType;
use App\Helper\CategoryHelper;
use App\Repository\CategoryRepository;
use App\Repository\NewsRepository;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/news")
 */
class NewsController extends AbstractController
{

    /**
     * @Route("/",name="news_index")
     */
    public function indexNews(Request $request, NewsRepository $newsRepository)
    {
        $isAdmin = $this->isGranted('ROLE_SUPER_ADMIN');

        if($search = $request->query->get('name'))
        {
            $news = $newsRepository->findNewsByName($search,$isAdmin);


        }
        else
        {
            $news = $newsRepository->findAllNews($isAdmin);

        }



        return $this->render('news/User/indexNews.html.twig', [
            'isNews'=> true,
            'news' => $news,
            'categories'=> CategoryHelper::$categoryList
        ]);
    }

    /**
     * @Route("/{slug}", name="news_page")
     * @return Response
     */
    public function pageNews(News $news)
    {

        return $this->render('news/pageNews.html.twig', [
            'news' => $news

        ]);
    }





}
