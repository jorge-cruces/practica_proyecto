<?php

namespace App\Controller\User;

use App\Form\LoginFormType;
use App\Form\UserFormPhpType;
use App\Helper\CategoryHelper;
use App\Repository\NewsRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
    /**
     * @Route("/", name="userHome")
     */
    public function index(NewsRepository $newsRepository): Response
    {
        $news = $newsRepository->findLastNewsByNumber(5);


        return $this->render('principal_view/index.html.twig', [
            'isHome' => true,
            'news' => $news,
            'categories' => CategoryHelper::$categoryList

        ]);
    }




}
