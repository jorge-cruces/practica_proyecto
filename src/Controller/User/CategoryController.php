<?php

namespace App\Controller\User;

use App\Entity\Category;
use App\Form\CategoryFormType;
use App\Helper\CategoryHelper;
use App\Repository\CategoryRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/categories")
 *
 */

class CategoryController extends AbstractController
{

    /**
     * @Route("/",name="category_index")
     */
    public function indexCategoryUser(Request $request,CategoryRepository $categoryRepository)
    {


        $isAdmin = $this->isGranted('ROLE_ADMIN');
        // TODO - Change isGranted userAnonymouly to Super_Admin and then delete this line

        if($categoryName = $request->query->get('name'))
        {
            $categories = $categoryRepository->findCategoryByName($categoryName,$isAdmin);
        }
        else
        {
            $categories = $categoryRepository->findAllCategories($isAdmin);
        }


        return $this->render('category/User/indexCategory.html.twig', [
            'isCategory' => true,
            'categories' => $categories
        ]);
    }

    /**
     * @Route("/{name}",name="category_news")
     * @param Category $category
     * @param CategoryRepository $categoryRepository
     * @return Response
     */
    public function categoryNews(Category $category)
    {

        $newsCategory = $category->getNews();

        return $this->render('category/User/newsCategory.html.twig',[
            'name' => $category->getName(),
            'news' => $newsCategory,
            'categories'=> CategoryHelper::$categoryList
        ]);


    }



}
