<?php


namespace App\Controller\Admin;


use App\Entity\News;
use App\Form\NewsFormType;
use App\Repository\NewsRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/news")
 */
class NewsAdminController extends AbstractController
{

    /**
     * @Route("/",name="news_index_admin")
     */
    public function indexNewsAdmin(Request $request, NewsRepository $newsRepository): Response
    {
        $isSuperAdmin = $this->isGranted('ROLE_SUPER_ADMIN');

        if($categoryName = $request->query->get('name'))
        {
            $news = $newsRepository->findNewsByName($categoryName,$isSuperAdmin);
        }
        else
        {
            $news = $newsRepository->findAllNews($isSuperAdmin);
        }


        return $this->render('news/Admin/indexNewsAdmin.html.twig', [
            'isNews'=> true,
            'news' => $news
        ]);
    }

    /**
     * @Route("/create", name="news_create_admin")
     */
    public function createNewsAdmin(Request $request, EntityManagerInterface $entityManager)
    {
        $newsForm = $this->createForm(NewsFormType::class);

        $newsForm->handleRequest($request);

        if($newsForm->isSubmitted() && $newsForm->isValid())
        {
            $news = $newsForm->getData();

            $entityManager->persist($news);
            $entityManager->flush();

            // TODO - Add flash cards
            return $this->redirectToRoute('news_index');

        }

        return $this->render('news/Admin/createNewsAdmin.html.twig', [
            'newsForm' => $newsForm->createView()
        ]);
    }

    /**
     * @Route("/edit/{slug}",name="news_edit_admin")
     * @param Request $request
     * @param News $news
     */
    public function editNewsAdmin(Request $request, News $news, EntityManagerInterface $entityManager)
    {
        $newsForm = $this->createForm(NewsFormType::class,$news,['edit' => true]);

        $newsForm->handleRequest($request);

        if($newsForm->isSubmitted() && $newsForm->isValid())
        {
            $news = $newsForm->getData();

            $entityManager->persist($news);
            $entityManager->flush();

            // TODO - Add flash cards
            return $this->redirectToRoute('news_index_admin');

        }
        return $this->render('news/Admin/editNewsAdmin.html.twig', [
            'newsTitle' => $news->getTitle(),
            'newsForm' => $newsForm->createView()
        ]);


    }






}