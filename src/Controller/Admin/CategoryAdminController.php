<?php


namespace App\Controller\Admin;


use App\Entity\Category;
use App\Form\CategoryFormType;
use App\Repository\CategoryRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/categories")
 *
 */

class CategoryAdminController extends AbstractController
{


    /**
     * @Route("/",name="category_index_admin")
     * @param Request $request
     * @param CategoryRepository $categoryRepository
     * @return Response
     */
    public function indexCategoryAdmin(Request $request,CategoryRepository $categoryRepository): Response
    {


        $isAdmin = $this->isGranted('ROLE_ADMIN');
        // TODO - Change isGranted userAnonymouly to Super_Admin and then delete this line

        if($categoryName = $request->query->get('name'))
        {
            $categories = $categoryRepository->findCategoryByName($categoryName,$isAdmin);
        }
        else
        {
            $categories = $categoryRepository->findAllCategories($isAdmin);
        }


        return $this->render('category/Admin/indexCategoryAdmin.html.twig', [
            'isCategory' => true,
            'categories' => $categories
        ]);
    }


    /**
     *
     * @Route("/create/",name="category_create_admin")
     */
    public function createCategoryAdmin(Request $request): Response
    {
        $entityManager = $this->getDoctrine()->getManager();

        $categoryForm = $this->createForm(CategoryFormType::class);
        $categoryForm->handleRequest($request);

        if($categoryForm->isSubmitted() && $categoryForm->isValid())
        {

            $category = $categoryForm->getData();
            $entityManager->persist($category);
            $entityManager->flush();

            // TODO - add flash card
            return $this->redirectToRoute('category_index');

        }


        return $this->render('category/Admin/createCategoryAdmin.html.twig', [
            'isCategory' => true,
            'categoryForm' => $categoryForm->createView()

        ]);
    }


    /**
     * @Route("/edit/{id}",name="category_edit_admin")
     */
    public function editCategoryAdmin(Category $category, Request $request): Response
    {
        $entityManager = $this->getDoctrine()->getManager();

        $categoryForm = $this->createForm(CategoryFormType::class,$category);
        $categoryForm->handleRequest($request);

        if($categoryForm->isSubmitted() && $categoryForm->isValid())
        {

            $entityManager->persist($category);
            $entityManager->flush();

            // TODO - add flash card
            return $this->redirectToRoute('category_index');

        }


        return $this->render('category/Admin/editCategoryAdmin.html.twig', [
            'isCategory' => true,
            'categoryForm' => $categoryForm->createView()

        ]);
    }


    /**
     * @Route("/{name}",name="category_news_admin")
     * @param Category $category
     * @return Response
     */
    public function categoryNewsAdmin(Category $category): Response
    {

        $newsCategory = $category->getNews();

        return $this->render('category/Admin/newsCategoryAdmin.html.twig',[
            'name' => $category->getName(),
            'news' => $newsCategory
        ]);


    }



}