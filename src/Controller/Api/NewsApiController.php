<?php


namespace App\Controller\Api;


use App\Entity\Category;
use App\Entity\News;
use App\Repository\NewsRepository;
use Doctrine\ORM\EntityManagerInterface;
use dsarhoya\BaseBundle\Controller\BaseController;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * @Route("/api/1")
 */
class NewsApiController extends ApiBaseController
{

    // GET METHOD

    /**
     * @Route("/news",methods={"GET"})
     */
    public function getAllNews(NewsRepository $newsRepository)
    {
        $categories = $newsRepository->findAll();

        return $this->serializedResponse($categories,['new_list']);

    }


    /**
     * @Route("/news/{id}",methods={"GET"})
     * @param News $news
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getSpecificNews(News $news)
    {


        return $this->serializedResponse($news,['new_detail','new_img']);
    }



    // POST METHODS
    /**
     * @Route("/news", methods={"POST"})
     * @return \Symfony\Component\HttpFoundation\Response
     */
    /*
    public function removeSpecificCategory(Request $request, SerializerInterface $serializer, EntityManagerInterface $entityManager)
    {

        $infoNews = $request->getContent();

        $news = $serializer->deserialize($infoNews,News::class,'json');
        $entityManager->persist($news);
        $entityManager->flush();

        return $this->serializedResponse($news,['groups'=>'show_category']);
    }
*/



}