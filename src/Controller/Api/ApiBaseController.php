<?php


namespace App\Controller\Api;




use JMS\Serializer\SerializationContext;
use JMS\Serializer\SerializerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;



class ApiBaseController extends AbstractController
{

    private $serializer;

    public function __construct(SerializerInterface $serializer)
    {

        $this->serializer = $serializer;
    }

    public function serializedResponse($data, $groups = [], $statusCode = 200): Response
    {
        $request = $this->get('request_stack')->getCurrentRequest();
        if (is_array($request->get('expand'))) {
            $groups = array_merge($groups, $request->get('expand'));
        } elseif (is_array($request->get('expand[]'))) {
            $groups = array_merge($groups, $request->get('expand[]'));
        }

        $context = is_array($groups) && count($groups) ? SerializationContext::create()->setGroups($groups) : SerializationContext::create();

        $response = new Response($this->serializer->serialize($data, 'json', $context), $statusCode);

        $response->headers->set('Content-Type', 'application/json');


        return $response;
    }


}