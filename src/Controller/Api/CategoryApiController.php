<?php


namespace App\Controller\Api;


use App\Entity\Category;
use App\Form\CategoryFormType;
use App\Repository\CategoryRepository;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * @Route("/api/1")
 */
class CategoryApiController extends ApiBaseController
{

    // GET METHODS

    /**
     * @Route("/categories",methods={"GET"})
     */
    public function getAllCategories(CategoryRepository $categoryRepository)
    {
        $categories = $categoryRepository->findAll();

        return $this->serializedResponse($categories,['category_list']);

    }


    /**
     * @Route("/categories/{id}",methods={"GET"})
     * @param Category $category
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getSpecificCategory(Category $category)
    {


        return $this->serializedResponse($category,['category_detail','new_img']);
    }

/*
    // PUT METHODS
    /**
     * @Route("/categories/{id}", methods={"PUT"})
     * @return \Symfony\Component\HttpFoundation\Response
     */
    /*public function editSpecificCategory(Category $category,Request $request, SerializerInterface $serializer, EntityManagerInterface $entityManager)
    {

        if(!$category){
            throw $this->createNotFoundException("Not found");
        }

        $data = $serializer->deserialize($request->getContent());
        $form = $this->createForm(new CategoryFormType(), $category);
        $form->submit($data);

        $entityManager->persist($category);
        $entityManager->flush();


        return $this->serializedResponse($category,['groups'=>'show_category']);
    }
    */
}