<?php

namespace App\Entity;

use App\Repository\NewsRepository;
use Doctrine\ORM\Mapping as ORM;
use dsarhoya\DSYFilesBundle\Interfaces\IFileEnabledEntity;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=NewsRepository::class)
 */
class News implements IFileEnabledEntity
{

    use TimestampableEntity;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @JMS\Groups({"new_list","new_detail"})
     *
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotNull
     * @Assert\Type("string")
     * @JMS\SerializedName("title")
     * @JMS\Groups({"new_list","new_detail","r_category_new"})
     */
    private $title;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Assert\NotNull
     * @Assert\NotBlank
     * @JMS\SerializedName("body")
     * @JMS\Groups({"new_detail"})
     */
    private $body;

    /**
     * @ORM\Column(type="boolean")
     * @Assert\NotNull
     * @JMS\SerializedName("enabled")
     * @JMS\Groups({"new_detail"})
     */
    private $enabled;

    /**
     * @ORM\ManyToOne(targetEntity=Category::class, inversedBy="news")
     * @ORM\JoinColumn(nullable=false)
     * @Assert\NotNull
     * @JMS\SerializedName("category")
     * @JMS\Groups({"r_new_category"})
     */
    private $category;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\NotNull
     * @Assert\NotBlank
     * @JMS\SerializedName("description")
     * @JMS\Groups({"new_detail","r_category_new"})
     *
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Type("string")
     */
    private $img;

    /**
     * @Gedmo\Slug(fields={"title"})
     * @ORM\Column(length=128, unique=true)
     */
    private $slug;

    /**
     * @var \Symfony\Component\HttpFoundation\File\UploadedFile
     * @Assert\Image(
     *      mimeTypesMessage = "No es una imagen."
     * )
     */
    private $file;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getBody(): ?string
    {
        return $this->body;
    }

    public function setBody(?string $body): self
    {
        $this->body = $body;

        return $this;
    }

    public function getEnabled(): ?bool
    {
        return $this->enabled;
    }

    public function setEnabled(bool $enabled): self
    {
        $this->enabled = $enabled;

        return $this;
    }

    public function getCategory(): ?Category
    {
        return $this->category;
    }

    public function setCategory(?Category $category): self
    {
        $this->category = $category;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getImg(): ?string
    {
        return $this->img;
    }

    public function setImg(?string $img): self
    {
        $this->img = $img;

        return $this;
    }

    public function getSlug()
    {
        return $this->slug;
    }


    public function setFile($file)
    {
        $this->file = $file;
        $this->setImg(sprintf('news_%s', md5(time())));

        return $this;
    }


    public function getFile()
    {
        return $this->file;
    }

    public function getFileKey()
    {
        return $this->img ? $this->img : null;
    }

    public function getFilePath()
    {
        return 'news';
    }

    public function getFileProperties()
    {
        return ['ACL' => 'public-read'];
    }

    public function getFullPathAndKey()
    {
        return $this->getFilePath().'/'.$this->getImg();
    }

}
