<?php

namespace App\EventSubscriber;

use App\Entity\Device;
use App\Entity\News;
use App\Entity\PillReminder;
use App\Entity\PillReminderTime;
use App\Entity\Post;
use App\Entity\User;
use App\Entity\WebappAccess;
use App\Service\DateTimeService;
use App\Service\WebappAccessService;
use dsarhoya\DSYFilesBundle\Services\DSYFilesService;
use JMS\Serializer\EventDispatcher\Events;
use JMS\Serializer\EventDispatcher\EventSubscriberInterface;
use JMS\Serializer\EventDispatcher\ObjectEvent;
use JMS\Serializer\JsonSerializationVisitor;
use JMS\Serializer\Metadata\StaticPropertyMetadata;

class SerializerSubscriber implements EventSubscriberInterface
{
    protected $filesSrv;


    public function __construct(DSYFilesService $filesSrv)
    {
        $this->filesSrv = $filesSrv;

    }

    public static function getSubscribedEvents()
    {
        // return the subscribed events, their methods and priorities
        return [
            ['event' => Events::POST_SERIALIZE, 'method' => 'onPostSerialize'],
        ];
    }

    public function onPostSerialize(ObjectEvent $event)
    {
        $entity = $event->getObject();
        /** @var JsonSerializationVisitor $visitor */
        $visitor = $event->getVisitor();

        $context = $event->getContext();

        if($entity instanceof News) {
            if (in_array('new_img', $context->getAttribute('groups'))) {
                $visitor->visitProperty(new StaticPropertyMetadata('','img_url',$this->filesSrv->fileUrl($entity->getFullPathAndKey(), ['signed' => true])), $this->filesSrv->fileUrl($entity->getFullPathAndKey(), ['signed' => true]));


            }
        }
/*
        if ($entity instanceof User) {
            if (in_array('user_api_key', $context->getAttribute('groups'))) {
                $visitor->setData('api_key', $entity->getApiKey());
            }
        }

        if ($entity instanceof Device) {
            if (in_array('device_api_key', $context->getAttribute('groups'))) {
                $visitor->setData('device_api_key', $entity->getApiKey());
            }
        }

        if ($entity instanceof Post) {
            if (null !== $entity->getFileKey()) {
                $visitor->setData('image_url', $this->filesSrv->fileUrl($entity->getFullPathAndKey(), ['signed' => true]));
            }

            if (null !== $entity->getVideoKey()) {
                $visitor->setData('video_url', $this->filesSrv->fileUrl($entity->getVideoKey(), ['signed' => true]));
            }
        }

        if ($entity instanceof WebappAccess) {
            $visitor->setData('webapp_access_url', $this->webappAccessSrv->getUrlWebappAccess($entity));
        }

*/
    }
}
