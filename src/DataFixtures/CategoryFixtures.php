<?php


namespace App\DataFixtures;


use App\Entity\Category;

use App\Helper\CategoryHelper;
use Doctrine\Persistence\ObjectManager;

class CategoryFixtures extends BaseFixture
{



    protected function loadData(ObjectManager $em)
    {
        foreach(CategoryHelper::$categoryList as $categoryName)
        {
            $category = new Category();
            $category->setName($categoryName);
            $category->setEnabled($this->faker->boolean(70));
            $category->setCreatedAt($this->faker->dateTimeBetween('-100 days', '-1 days'));
            $em->persist($category);
        }

        $em->flush();
    }
}