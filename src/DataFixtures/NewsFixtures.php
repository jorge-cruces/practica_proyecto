<?php


namespace App\DataFixtures;


use App\Entity\News;
use App\Repository\CategoryRepository;
use Doctrine\Persistence\ObjectManager;

class NewsFixtures extends BaseFixture
{

    /**
     * @var CategoryRepository
     */
    private $categoryRepository;

    public function __construct(CategoryRepository $categoryRepository)
    {

        $this->categoryRepository = $categoryRepository;
    }

    protected function loadData(ObjectManager $em)
    {

        $categories = $this->categoryRepository->findAll();

        for($i = 0; $i < 25; $i++)
        {
            $news = new News();
            $news->setTitle($this->faker->text(20));
            $news->setImg($this->faker->imageUrl(750,300));
            $news->setDescription($this->faker->text(50));
            $news->setEnabled($this->faker->boolean(70));
            $news->setCreatedAt($this->faker->dateTimeBetween('-100 days', '-1 days'));
            $news->setBody($this->faker->text(1000));
            $news->setCategory($this->faker->randomElement($categories));
            $em->persist($news);
        }
        $em->flush();
    }
}